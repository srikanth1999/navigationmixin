import { LightningElement,wire,track } from 'lwc';
import { CurrentPageReference } from "lightning/navigation";
import { NavigationMixin } from "lightning/navigation";

export default class LwcNavigationMixin extends NavigationMixin(LightningElement) {
    @wire(CurrentPageReference) pageRef;
    @track tabName;
    
    handleClick() {
        this[NavigationMixin.Navigate]({
            type: 'standard__navItemPage',
            attributes: {
                apiName: this.tabName,
            },
        });
    }
 
}